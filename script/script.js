/*Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
Події в JavaScript - це механізм, який дозволяє вам реагувати на дії користувачів або на інші події, які виникають у додатку або на веб-сторінці. 
Події можуть бути викликані різними діями, наприклад клік миші, натискання клавіш, завантаження сторінки, зміна розміру вікна браузера тощо.
Події використовуються для створення інтерактивних та реактивних додатків, які можуть реагувати на взаємодію користувача та зміни в середовищі виконання.

2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
click - клік на ліву кнопку миші;
contextmenu - клік на праву кнопку миші;
mousedown - натискання на кнопку миші;
mouseup - відпускання кнопки миші;
mouseover - коли курсор наводиться на об'єкт;
mouseout - коли курсор покидає об'єкт;
mousemove - рух курсора по об'єкту.

3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
contextmenu - це подія, коли користувач правою кнопкою миші клікає на елемент для того, щоб відкрити контекстне меню.

Практичні завдання
1. Додати новий абзац по кліку на кнопку: По кліку на кнопку <button id="btn-click">Click Me</button>, 
створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
*/

const content = document.getElementById('content');
const btn = document.getElementById('btn-click');
btn.addEventListener('click', () => {
    const paragraph = document.createElement('p');
    paragraph.textContent = 'New Paragraph';
    content.prepend(paragraph);
})

/*2. Додати новий елемент форми із атрибутами: Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, 
наприклад, type, placeholder, і name. та додайте його під кнопкою.*/

const createBtn = document.createElement('button');
createBtn.setAttribute('id', 'btn-input-create');
createBtn.textContent = 'Create input';
content.append(createBtn);
createBtn.style.cssText = 'display: block; margin: 30px auto; background-color: #007bff; color: #fff; padding: 10px 20px; border: none; cursor: pointer;'

createBtn.addEventListener('click', () => {
    const input = document.createElement('input');
    input.setAttribute('type', 'text');
    input.setAttribute('placeholder', 'Write your Name!');
    input.setAttribute('name', 'Name');
    content.append(input);
})